namespace Aoc2018
{
    public interface IDay
    {
        string Part1(string rawInput);
        string Part2(string rawInput);
    }
}