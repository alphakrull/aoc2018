using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Aoc2018
{
    public class Day1 : IDay
    {
        public string Part1(string rawData)
        {
            var input = Parser.SplitByNewLine(rawData);

            return input.Sum(_ => int.Parse(_)).ToString();
        }

        public string Part2(string rawData)
        {
            var input = Parser.SplitByNewLine(rawData);
            var visited = new List<int>{0};
            var newFreq = 0;
            foreach(var val in input.Cycle())
            {
                newFreq += int.Parse(val);
                if(visited.Contains(newFreq))
                {
                    return newFreq.ToString();
                }
                visited.Add(newFreq);
            }
            return "";
        }
    }
}