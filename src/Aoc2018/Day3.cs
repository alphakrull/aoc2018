using System;
using System.Collections.Generic;
using System.Linq;

namespace Aoc2018
{
    public class Day3 : IDay
    {
        public string Part1(string rawInput)
        {
            string[,] fabric = new string[1000, 1000];

            var claims = Parser.SplitByNewLine(rawInput.Trim())
                .Select(_ => _.Split(new[] { " @ ", ",", ": ", "x" }, StringSplitOptions.None))
                .Select(parts => (id: parts[0], marginLeft: int.Parse(parts[1]), marginTop: int.Parse(parts[2]), width: int.Parse(parts[3]), height: int.Parse(parts[4])));

            int overlap = 0;

            foreach(var claim in claims)
            {
                for (var j = claim.marginTop; j < claim.marginTop + claim.height; j++)
                {
                    for(var k = claim.marginLeft; k < claim.marginLeft + claim.width; k++)
                    {
                        if (string.IsNullOrEmpty(fabric[j, k]))
                        {
                            fabric[j, k] = claim.id;
                        }
                        else if(fabric[j, k] != "X")
                        {
                            fabric[j, k] = "X";
                            overlap++;
                        }
                    }
                }
            }

            return overlap.ToString();
        }

        public string Part2(string rawInput)
        {
            string[,] fabric = new string[1000, 1000];

            var claims = Parser.SplitByNewLine(rawInput.Trim())
                .Select(_ => _.Split(new[] { " @ ", ",", ": ", "x" }, StringSplitOptions.None))
                .Select(_ => (id: _[0], marginLeft: int.Parse(_[1]), marginTop: int.Parse(_[2]), width: int.Parse(_[3]), height: int.Parse(_[4])));

            var claimedSquaresById = new Dictionary<string, int>();

            foreach(var claim in claims)
            {
                claimedSquaresById.Add(claim.id, 0);
                for (var j = claim.marginTop; j < claim.marginTop + claim.height; j++)
                {
                    for (var k = claim.marginLeft; k < claim.marginLeft + claim.width; k++)
                    {
                        if (string.IsNullOrEmpty(fabric[j, k]))
                        {
                            claimedSquaresById[claim.id]++;
                            fabric[j, k] = claim.id;
                        }
                        else if (fabric[j, k] != "X")
                        {
                            claimedSquaresById[fabric[j, k]]--;
                            fabric[j, k] = "X";
                        }
                    }
                }
            }
            return claims
                .Single(_ => claimedSquaresById[_.id] == (_.width * _.height)).id;
        }
    }
}