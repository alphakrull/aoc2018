using System.IO;
using System.Text;
using System;

namespace Aoc2018
{
    public static class Parser
    {
        public static string ReadFile(string fileName)
        {
            return File.ReadAllText(fileName, Encoding.UTF8);
        }

        public static string[] SplitByNewLine(string data)
        {
            return data.Split(Environment.NewLine);
        }
    }
}