using Aoc2018;
using System;
using Xunit;

namespace Aoc2018.Tests
{
    public class Day2Tests
    {
        [Theory]
        [InlineData("abcdef\r\nbababc\r\nabbcde\r\nabcccd\r\nabcdee\r\n\ababab", "12")]
        public void Part1(string data, string expected)
        {
            var day = new Day2();

            var actual = day.Part1(data);
            
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("abcde\r\nfghij\r\nklmno\r\npqrst\r\nfguij\r\naxcye\r\nwvxyz", "fgij")]
        public void Part2(string data, string expected)
        {
            var day = new Day2();

            var actual = day.Part2(data);
            
            Assert.Equal(expected, actual);
        }
    }
}
